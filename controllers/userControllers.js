const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const Course = require("../models/Course");


//bcrypt is a package which allows us to hash our passwords to add a layer of security for our user's details

//Check if the email already exists
/*
	Steps"
	1. Use mongoose "find" method to find duplicate emails
	2.Use the "then" method to send a response back to the FE application based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqBody) =>{
	return User.find({email:reqBody.email}).then(result=>{
		if(result.length>0){
			// return true;
			result.password = " ";
			return result;
		}else{
			return false;
		};
	});
};

//User Registration
/*
	Steps:
	1. Create a new User object using the mongoose modela and the info from the reqBody
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/

module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password,10)
	})

	return newUser.save().then((user,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	});
};

//User Authentication
/*
	Steps:
	1. Check the database if the user email exists
	2.Compare the password in the login form with the password stored in the database
	3. Generate/return a JSON Web token if the user is successfully logged in and return false if not

*/

module.exports.loginUser = (reqBody) =>{
	return User.findOne({email:reqBody.email}).then(result=>{
		if(result==null){
			return false;
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result)}
			}else{
				return false
			};
		};
	});
};

//Retrieve user details
/*
	Steps:
	1. Find the document in the db using the user's ID
	2. Reassign the pw of the returned document to an empty string
	3. Return the result back to the frontend
*/
//Solution 1

// module.exports.getProfile = (reqBody) =>{
// 	return User.findById(reqBody.id).then(result=>{

// 		if(result==null){
// 			return false
// 		}else{
// 		result.password = "";
// 		return result;
// 	}
		
// 	})
// }

module.exports.getProfile = (data) =>{
	return User.findById(data.userId).then(result=>{
		result.password = " ";
		return result;
	})
}

//  enroll the user to a class
/*
	1. find the Doc in th DB using the User's ID
	2. Add the courseID to the user's enrollment array
	3. update the docment in to mongoDB

	1st, find the user who is enrolling and updates his enrollment subdocument array. we will push the courseID in th enrollments array

	2nd, find the course where we are enrolling and update its enrolless array. we will push the userID in the enrolles array
	
	3rd since we will access 2 collections in one action --  we will have to wait for the completion of the action instead of letting JS conticue line per line

	to be able to wait for the result of a function , we use await keyword . the await keywaord allows us to wait for the function to finish and get result before proceeding

*/

module.exports.enroll = async (data) =>{
	let isUserUpdated = await User.findById(data.userId).then(user=>{
		user.enrollments.push({courseId:data.courseId});

		return user.save().then((user, error)=>{
			if(error){
				return false;
			}else{
				return true;
			};
		});
	});

	let isCourseUpdated = await Course.findById(data.courseId).then(course=>{
		course.enrollees.push({userId:data.userId});
		return course.save().then((course,error)=>{
			if(error){
				return false;
			}else{
				return true;
			};
		});
	});

	if(isCourseUpdated && isCourseUpdated){
		return true;
	}else{
		return false;
	}
};