const Course = require("../models/Course");

//Mini Activity
//Create a new course
/*
	Steps:
	1.Create a new Course object using the mongoose model and the information from the reqBody
		name
		description
		price
	2.Save the new User to the database
	3. Send a screenshot of 3 courses from your database

*/

// module.exports.addCourse = (reqBody) => {

// 	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
// 	// Uses the information from the request body to provide all the necessary information
// 	let newCourse = new Course({
// 		name : reqBody.name,
// 		description : reqBody.description,
// 		price : reqBody.price
// 	});

// 	// Saves the created object to our database
// 	return newCourse.save().then((course, error) => {

// 		// Course creation successful
// 		if (error) {

// 			return false;

// 		// Course creation failed
// 		} else {

// 			return true;

// 		};

// 	});

// };

// // solution 1
// module.exports.addCourse = (reqBody) => {

// 	let newCourse = new Course({
// 		name : reqBody.name,
// 		description : reqBody.description,
// 		price : reqBody.price
// 	});

// 	if(reqbody.isAdmin == true){ 

		
// 		return newCourse.save().then((course, error) => {

// 			if (error) {
	
// 				return false;
	
// 			} else {
	
// 				return true;
	
// 			};
	
// 		});

//    }else{
// 	   return false
//    }
	

// };

module.exports.addCourse = (data) => {

	console.log(data);

	if(data.isAdmin){

		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		console.log(newCourse);
		return newCourse.save().then((course, error) => {

		if (error) {

			return false;

		} else {

			return true;

		};

	});

	}else{
		return false;
	};
};


// Retrieve all courses
/*
	1. Retrieve all the courses from Database
*/
module.exports.getAllCourses=()=>{
	return Course.find({}).then(result=>{
		return result;
	})
}


// retrive all active courses from Database
/*
	1.retrive all the courses from the DB w/ the property of isActive= true
*/

module.exports.getAllActive=()=>{
	return Course.find({isActive:true}).then(result=>{

		return result;
	})
}

// Retrieving specific course
/*
	1.retrieve the course that matches the course iD provided from URL
*/
module.exports.getCourse=(reqParams)=>{
	return Course.findById(reqParams.courseId).then(result=>{
		return result;
	})
}

// Updating course
/*
 create a variable "updatedCourse" wc/ will contain the infromation retrived form the request body
 find and update the course using the course ID retrieved form the request params property and var "updated course" containing  the information form the request
*/
module.exports.updateCourse = (reqParams,reqBody)=>{
	let updateCourse ={
	name: reqBody.name,
	description:reqBody.description,
	price: reqBody.price,
	isActive: reqBody.isActive
	};
	return Course.findByIdAndUpdate(reqParams.courseId,updateCourse).then((course,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	});
};



// update status

module.exports.updateStatus = (reqParams,reqBody)=>{
	let updateStatus ={
	isActive: reqBody.isActive,
	};
	return Course.findByIdAndUpdate(reqParams.courseId,updateStatus).then((course,error)=>{
		if(error){
			return false;
		}else{
			return true;
		};
	});
};













