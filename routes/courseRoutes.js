const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");

//Route for creating a course
//refactor this route to implement user authentication for our admin when creating a course

// router.post("/", (req, res) => {
// 	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
// })





router.get("/",auth.verify,(req,res)=>{

	const data ={
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController=>res.send(resultFromController))
});


// route to retrive all tye courses.
router.get("/all",(req,res)=>{
	courseController.getAllCourses().then(resultFromController=>res.send(resultFromController));
});


// retriving active courses
router.get("/active",(req,res)=>{
	courseController.getAllActive().then(resultFromController=>res.send(resultFromController));
});

// retrive using ID
router.get("/:courseId",(req,res)=>{
	courseController.getCourse(req.params).then(resultFromController=>res.send(resultFromController));
});


// updateCourse
router.put("/:courseId",auth.verify,(req,res) =>{
	courseController.updateCourse(req.params,req.body).then(resultFromController=>res.send(resultFromController));
});


// update status
router.put("/:courseId",auth.verify,(req,res) =>{
	courseController.updateCourse(req.params,req.body).then(resultFromController=>res.send(resultFromController));
});


// update status
router.put("/:id:", auth.verify,(req,res)=>{
	courseController.updateStatus(req.params,req.body).then(resultFromController=>res.send(resultFromController));
})
module.exports = router;


